import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

// 建立store
const store =  new Vuex.Store({
    // 1.
    // 接收來自VUE COMPONENTS元件發送（dispatch）的事件，並且提交（commit）事件給 Mutations 去執行他該做的事情
    actions: {
        getIronmanData (context) {
            // 撈取 backend api
            fetch('https://raw.githubusercontent.com/shawnlin0201/ironmanData/master/ironman.json')
            .then(res => res.json())
            .then(res => {
                context.commit('setItonmanData', res)
            })
        }
    },  

    // 2.
    // 接收來自 Actions 的事件，並且發送變更（Mutate）給 store 裡本身的 State。
    mutations: {
        setItonmanData (state, json) {
            state.ironmanData = json
        }
    },
    
    // 3.
    // 接收來自 Mutations 發送的變更，更改 State 自己本身的狀態
    state: {
        ironmanData: {}
    }
    
})
export default store;